package com.example.bookreader;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

public class ReadingChoice extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reading_choice);
        RelativeLayout clickBook =(RelativeLayout)findViewById(R.id.book_choice);
        clickBook.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){openBooks();}
        });

        RelativeLayout clickMagazine =(RelativeLayout)findViewById(R.id.magazine_choice);
        clickMagazine.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){openMagazines();}
        });
    }


    private void openBooks() {
        Intent intent = new Intent(this, BookList.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    private void openMagazines() {
        Intent intent = new Intent(this, MagazineList.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }
}
