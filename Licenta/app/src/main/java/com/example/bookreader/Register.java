package com.example.bookreader;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bookreader.DAO.UserDAO;
import com.example.bookreader.Entities.User;

public class Register extends AppCompatActivity {

    private Button submitUpButton;
    private TextView forgotPassword;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        submitUpButton = findViewById(R.id.submit_data);
        submitUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLogIn();
            }
        });

        forgotPassword = findViewById(R.id.forgot_password_link2);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openForgotPassword();
            }


        });
    }

    public void openLogIn() {
        UserDAO userDAO = new UserDAO();
        EditText email = findViewById(R.id.register_email_input);
        EditText username = findViewById(R.id.register_username_input);
        EditText password = findViewById(R.id.register_password_input);
        EditText repeatPassword = findViewById(R.id.register_repeat_password_input);
        User user_name = userDAO.searchUsername(username.getText().toString());
        User user_email = userDAO.searchEmail(email.getText().toString());
        if(!email.getText().toString().equals("") &&
                !username.getText().toString().equals("") &&
                !password.getText().toString().equals("") &&
                !repeatPassword.getText().toString().equals("")) {
            if (email.getText().toString().equals(user_email.getEmail()) || username.getText().toString().equals(user_name.getUsername())) {
                Toast.makeText(Register.this, "Account already exists using the email/username!", Toast.LENGTH_SHORT).show();
            } else {
                if (password.getText().toString().equals(repeatPassword.getText().toString())){
                    User newUser = new User(username.getText().toString(), email.getText().toString(), password.getText().toString());
                    userDAO.newUser(newUser);
                    Intent intent = new Intent(this, LogIn.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                } else {
                    Toast.makeText(Register.this, "The passwords do not match!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        else{
            Toast.makeText(Register.this, "You must fill all credentials!", Toast.LENGTH_SHORT).show();
        }


    }

    private void openForgotPassword() {
        Intent intent = new Intent(this, ForgotPassword.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }
}
