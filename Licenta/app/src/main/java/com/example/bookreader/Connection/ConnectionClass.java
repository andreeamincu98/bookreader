package com.example.bookreader.Connection;

import android.annotation.SuppressLint;
import android.os.StrictMode;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionClass {
    String ip, port, database, username, password;
    @SuppressLint("NewApi")
    public Connection initializeConnection() throws SQLException {
        ip = "192.168.100.2";
        port = "1433";
        database = "BookReader";
        username = "bookreader";
        password = "1234";
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Connection connection = null;
        String connectionURL = null;

        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            connectionURL = "jdbc:jtds:sqlserver://" + ip + ":" + port + ";databasename=" + database + ";user=" + username + ";password=" + password + ";";
            connection = DriverManager.getConnection(connectionURL);
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
        return connection;
    }
}
