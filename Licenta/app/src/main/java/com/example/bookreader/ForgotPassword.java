package com.example.bookreader;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bookreader.DAO.UserDAO;
import com.example.bookreader.Entities.User;

public class ForgotPassword extends AppCompatActivity {

    private Button submitUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);

        submitUpButton= findViewById(R.id.update_credentials);
        submitUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLogIn();
            }
        });

    }

    public void openLogIn(){
        UserDAO userDAO = new UserDAO();
        EditText email = findViewById(R.id.register_email_input);
        EditText password = findViewById(R.id.register_password_input);
        EditText repeatPassword = findViewById(R.id.register_repeat_password_input);
        User user_email = userDAO.searchEmail(email.getText().toString());
        if(!email.getText().toString().equals("") &&
                !password.getText().toString().equals("") &&
                !repeatPassword.getText().toString().equals("")) {
                if (password.getText().toString().equals(repeatPassword.getText().toString())){
                    userDAO.updateUser(email.getText().toString(),password.getText().toString());
                    Intent intent=new Intent(this,LogIn.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                } else {
                    Toast.makeText(ForgotPassword.this, "The passwords do not match!", Toast.LENGTH_SHORT).show();
                }
        }
        else{
            Toast.makeText(ForgotPassword.this, "You must fill all credentials!", Toast.LENGTH_SHORT).show();
        }

    }
}
