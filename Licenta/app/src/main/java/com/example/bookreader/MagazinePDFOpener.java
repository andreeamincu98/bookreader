package com.example.bookreader;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class MagazinePDFOpener extends AppCompatActivity {

    PDFView myPDFViewer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.magazine_pdfopener);
        myPDFViewer=(PDFView) findViewById(R.id.magazine_pdfViewer);

        String getItem=getIntent().getStringExtra("pdfFileName");

        if(getItem.equals("Vogue Australia 2020")){{
            myPDFViewer.fromAsset("Vogue Australia 2020.pdf").load();
        }}
        if(getItem.equals("Vogue British 2020")){{
            myPDFViewer.fromAsset("Vogue British 2020.pdf").load();
        }}
        if(getItem.equals("Vogue Mexico 2020")){{
            myPDFViewer.fromAsset("Vogue Mexico 2020.pdf").load();
        }}
        if(getItem.equals("Vogue Poland 2020")){{
            myPDFViewer.fromAsset("Vogue Poland 2020.pdf").load();
        }}
        if(getItem.equals("Vogue Spain 2020")){{
            myPDFViewer.fromAsset("Vogue Spain 2020.pdf").load();
        }}
        if(getItem.equals("Vogue USA 2020")){{
            myPDFViewer.fromAsset("Vogue USA 2020.pdf").load();
        }}


    }
}
