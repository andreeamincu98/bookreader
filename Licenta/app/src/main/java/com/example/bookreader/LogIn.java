package com.example.bookreader;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bookreader.DAO.UserDAO;
import com.example.bookreader.Entities.User;

public class LogIn extends AppCompatActivity {

    private Button signUpButton;
    private Button signInButton;
    private TextView forgotPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        signUpButton= findViewById(R.id.sign_up);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRegister();
            }
        });

        signInButton= findViewById(R.id.sign_in);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBooks();
            }
        });

        forgotPassword = findViewById(R.id.forgot_password_link);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openForgotPassword();
            }


        });


    }

    public void openRegister(){
        Intent intent=new Intent(this,Register.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }


    public void openBooks(){

        UserDAO userDAO = new UserDAO();
        EditText email = findViewById(R.id.login_email_input);
        EditText password = findViewById(R.id.login_password_input);
        User user_name = userDAO.validUser(email.getText().toString(), password.getText().toString());
        if(!email.getText().toString().equals("") &&
                !password.getText().toString().equals("")) {
            if (email.getText().toString().equals(user_name.getEmail()) && password.getText().toString().equals(user_name.getPassword())) {
                Intent intent = new Intent(this, ReadingChoice.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            } else {
                Toast.makeText(LogIn.this, "Wrong credentials!", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(LogIn.this, "You must fill all credentials!", Toast.LENGTH_SHORT).show();
        }

    }


    private void openForgotPassword() {
        Intent intent = new Intent(this, ForgotPassword.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

}
