package com.example.bookreader.DAO;

import android.util.Log;

import com.example.bookreader.Connection.ConnectionClass;
import com.example.bookreader.Entities.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class UserDAO {
    private Vector<User> userVector = new Vector<>();
    private Connection connection;

    public Vector<User> getUsersFromDatabase() {

        try {
            ConnectionClass dbConnection = new ConnectionClass();
            connection = dbConnection.initializeConnection();
            if (connection != null) {
                String query = "SELECT * FROM [dbo].[Users]";
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);

                while (resultSet.next()) {
                    String username = resultSet.getString(2);
                    String email = resultSet.getString(3);
                    String password = resultSet.getString(4);
                    userVector.add(new User(username, email, password));
                }
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
        return userVector;
    }


    public static User currentUser = new User("user", "email", "pass");

    public User validUser(String email, String password) {

        try {
            ConnectionClass dbConnection = new ConnectionClass();
            connection = dbConnection.initializeConnection();
            if (connection != null) {
                String query = "SELECT * FROM [dbo].[Users] WHERE email =\'" + email + "\' and password = \'" + password + "\';";
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                while (resultSet.next()) {

                    currentUser.setUsername(resultSet.getString(2));
                    currentUser.setEmail(resultSet.getString(3));
                    currentUser.setPassword(resultSet.getString(4));

                    return new User(resultSet.getString(2), resultSet.getString(3), resultSet.getString(4));
                }
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
        return new User("user", "email", "pass");
    }


    public User searchUsername(String username) {
        try {
            ConnectionClass dbConnection = new ConnectionClass();
            connection = dbConnection.initializeConnection();
            if (connection != null) {
                String query = "SELECT * FROM [dbo].[Users] WHERE username =\'" + username + "\';";
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                while (resultSet.next()) {

                    currentUser.setUsername(resultSet.getString(2));
                    currentUser.setEmail(resultSet.getString(3));
                    currentUser.setPassword(resultSet.getString(4));

                    return new User(resultSet.getString(2), resultSet.getString(3), resultSet.getString(4));
                }
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
        return new User("user", "email", "pass");
    }


    public User searchEmail(String email) {
        try {
            ConnectionClass dbConnection = new ConnectionClass();
            connection = dbConnection.initializeConnection();
            if (connection != null) {
                String query = "SELECT * FROM [dbo].[Users] WHERE email =\'" + email + "\';";
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                while (resultSet.next()) {

                    currentUser.setUsername(resultSet.getString(2));
                    currentUser.setEmail(resultSet.getString(3));
                    currentUser.setPassword(resultSet.getString(4));

                    return new User(resultSet.getString(2), resultSet.getString(3), resultSet.getString(4));
                }
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
        return new User("user", "email", "pass");
    }


    public void updateUser(String email, String password) {
        try {
            ConnectionClass dbConnection = new ConnectionClass();
            connection = dbConnection.initializeConnection();
            if (connection != null) {
                User update_user = searchEmail(email.toString());
                String query = "UPDATE [dbo].[Users]\n" +
                        "SET [username]=\'" + update_user.getUsername().toString() +
                        "\'\n,[email]=\'" + email +
                        "\'\n,[password]=\'" + password +
                        "\'\n WHERE [email] =\'" + email + "\';";
                Statement statement = connection.createStatement();
                statement.execute(query);

            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }

    }


    public void newUser(User new_User) {

        String username = new_User.getUsername();
        String email = new_User.getEmail();
        String password = new_User.getPassword();
        try {
            ConnectionClass dbConnection = new ConnectionClass();
            connection = dbConnection.initializeConnection();
            if (connection != null) {
                String query = "INSERT INTO [dbo].[Users]\n" +
                        "           ([username]\n" +
                        "           ,[email]\n" +
                        "           ,[password])\n" +
                        "     VALUES(\'" + username + "\',\'" + email + "\',\'" + password + "\');";
                Statement statement = connection.createStatement();
                statement.execute(query);
                currentUser.setUsername(username);
                currentUser.setEmail(email);
                currentUser.setPassword(password);

            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
    }
}
