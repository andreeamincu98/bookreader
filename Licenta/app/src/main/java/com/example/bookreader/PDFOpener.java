package com.example.bookreader;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.github.barteksc.pdfviewer.PDFView;

public class PDFOpener extends AppCompatActivity {

    PDFView myPDFViewer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdfopener);
        myPDFViewer=(PDFView) findViewById(R.id.pdfViewer);

        String getItem=getIntent().getStringExtra("pdfFileName");

        if(getItem.equals("The Last Wish")){{
            myPDFViewer.fromAsset("The Last Wish.pdf").load();
        }}
        if(getItem.equals("Sword of Destiny")){{
            myPDFViewer.fromAsset("02. Sword of Destiny.pdf").load();
        }}
        if(getItem.equals("Blood of Elves")){{
            myPDFViewer.fromAsset("Blood of Elves.pdf").load();
        }}
        if(getItem.equals("Time of Contempt")){{
            myPDFViewer.fromAsset("Time of Contempt.pdf").load();
        }}
        if(getItem.equals("Baptism of Fire")){{
            myPDFViewer.fromAsset("Baptism of Fire.pdf").load();
        }}
        if(getItem.equals("The Tower of the Swallow")){{
            myPDFViewer.fromAsset("The Tower of the Swallow.pdf").load();
        }}
        if(getItem.equals("The Lady of the Lake")){{
            myPDFViewer.fromAsset("The Lady of the Lake.pdf").load();
        }}
        if(getItem.equals("Seasons of Storms")){{
            myPDFViewer.fromAsset("Seasons of Storms.pdf").load();
        }}
        if(getItem.equals("Vogue Australia 2020")){{
            myPDFViewer.fromAsset("Vogue Australia 2020.pdf").load();
        }}
        if(getItem.equals("Vogue British 2020")){{
            myPDFViewer.fromAsset("Vogue British 2020.pdf").load();
        }}
        if(getItem.equals("Vogue Mexico 2020")){{
            myPDFViewer.fromAsset("Vogue Mexico 2020.pdf").load();
        }}
        if(getItem.equals("Vogue Poland 2020")){{
            myPDFViewer.fromAsset("Vogue Poland 2020.pdf").load();
        }}
        if(getItem.equals("Vogue Spain 2020")){{
            myPDFViewer.fromAsset("Vogue Spain 2020.pdf").load();
        }}
        if(getItem.equals("Vogue USA 2020")){{
            myPDFViewer.fromAsset("Vogue USA 2020.pdf").load();
        }}

    }
}