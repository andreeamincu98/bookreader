package com.example.bookreader;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class BookPDFOpener extends AppCompatActivity {
    PDFView myPDFViewer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_pdfopener);
        myPDFViewer=(PDFView) findViewById(R.id.book_pdfViewer);

        String getItem=getIntent().getStringExtra("pdfFileName");

        if(getItem.equals("The Last Wish")){{
            myPDFViewer.fromAsset("The Last Wish.pdf").load();
        }}
        if(getItem.equals("Sword of Destiny")){{
            myPDFViewer.fromAsset("02. Sword of Destiny.pdf").load();
        }}
        if(getItem.equals("Blood of Elves")){{
            myPDFViewer.fromAsset("Blood of Elves.pdf").load();
        }}
        if(getItem.equals("Time of Contempt")){{
            myPDFViewer.fromAsset("Time of Contempt.pdf").load();
        }}
        if(getItem.equals("Baptism of Fire")){{
            myPDFViewer.fromAsset("Baptism of Fire.pdf").load();
        }}
        if(getItem.equals("The Tower of the Swallow")){{
            myPDFViewer.fromAsset("The Tower of the Swallow.pdf").load();
        }}
        if(getItem.equals("The Lady of the Lake")){{
            myPDFViewer.fromAsset("The Lady of the Lake.pdf").load();
        }}
        if(getItem.equals("Seasons of Storms")){{
            myPDFViewer.fromAsset("Seasons of Storms.pdf").load();
        }}


    }
}
